<?php

declare(strict_types=1);

namespace Warpcamp\Packages\A;

//
use Warpcamp\Packages\B\PackageB;
use Warpcamp\Packages\C\PackageC;

class PackageA
{
    public function asdf()
    {
        new PackageB();
        new PackageC();
    }
}